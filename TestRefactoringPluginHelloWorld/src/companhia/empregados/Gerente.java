package companhia.empregados;

import java.util.ArrayList;
import java.util.List;

public class Gerente {

	private double soldoBruto;
	private List<String> projetos;
	private boolean temPlanoSaude;
	
	public Gerente() {
		this.projetos = new ArrayList<String>();
	}

	public void setSoldoBruto(double soldoBruto) {
		this.soldoBruto = soldoBruto;
	}

	public double getSoldoLiquido() {
		double soldoLiquido = this.soldoBruto
							  - descontoSoldoLiquido()
							  - descontoPlanoSaude()
							  + adicionaisPorProjeto();
		
		return soldoLiquido;
	}

	public void adicionaProjeto(String projeto) {
		this.projetos.add(projeto);
	}

	public void setPlanoSaude(boolean temPlanoSaude) {
		this.temPlanoSaude = temPlanoSaude;
	}

	private double descontoSoldoLiquido() {
		return this.soldoBruto * 0.25;
	}
	
	private double adicionaisPorProjeto() {
		int quantidadeDeProjetos = this.projetos.size();
		return 200 * quantidadeDeProjetos;
	}
	
	private double descontoPlanoSaude() {
		if(temPlanoSaude)
			return this.soldoBruto * 0.05;
		else
			return 0;
	}
	
}
