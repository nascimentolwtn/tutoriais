package companhia.empregados.test;

import junit.framework.TestCase;

import companhia.empregados.Gerente;

public class TestGerenteRefatoracaoMergeIncrementalSetUpTearDownCreateFixture extends TestCase {

   Gerente gerente;

   public void setUp(){
      gerente = new Gerente();
      gerente.setSoldoBruto(3000.00);
   }

   public void testSalarioSemProjetos() {
      assertTrue("Soldo menos 25%",
    		  	gerente.getSoldoLiquido() == 2250.00);
   }

   public void testSalarioProjeto() {
      gerente.adicionaProjeto("Projeto 1");
      assertTrue("Soldo menos 25% mais 200 por projeto",
    		  	gerente.getSoldoLiquido() == 2450.00);
   }

   public void testSalarioProjetos() {
      gerente.adicionaProjeto("Projeto 1");
      gerente.adicionaProjeto("Projeto 2");
      assertTrue("Soldo menos 25% mais 200 por projeto",
    		   	gerente.getSoldoLiquido() == 2650.00);
   }

   public void testPlanoSaude() {
      gerente.setPlanoSaude(true);
      assertTrue("Soldo menos 25% menos 5%",
    		  	gerente.getSoldoLiquido() == 2100.00);
   }

   public void tearDown(){
      gerente = null;
   }

}
