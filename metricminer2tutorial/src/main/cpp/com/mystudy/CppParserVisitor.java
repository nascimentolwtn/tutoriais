package com.mystudy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.List;

import org.apache.commons.io.IOUtils;

import br.com.metricminer2.domain.Commit;
import br.com.metricminer2.persistence.PersistenceMechanism;
import br.com.metricminer2.scm.CommitVisitor;
import br.com.metricminer2.scm.RepositoryFile;
import br.com.metricminer2.scm.SCMRepository;

public class CppParserVisitor implements CommitVisitor {

	public void process(SCMRepository repo, Commit commit, PersistenceMechanism writer) {
		
		if(!isProcessableHash(commit.getHash())) return;
		
		List<RepositoryFile> files = this.getRepositoryFiles(repo, commit.getHash());

		for(RepositoryFile file : files) {
			File soFile = file.getFile();

			if(!isFileForStudy(soFile)) continue;
			
			LineNumberReader lineNumberReader = null;
			try {
				lineNumberReader = new LineNumberReader(new FileReader(soFile));
				
				long lineCount = 0, LOC = 0, eLOC = 0;
				
				String line = lineNumberReader.readLine();
				while(line != null) {
					lineCount++;
					if(!line.isEmpty()) {
						LOC++;
						if(!line.startsWith("/*")) {
							eLOC++;
						}
					} 
					line = lineNumberReader.readLine();
				}
				
				writer.write(
						commit.getHash(),
						file.getFullName(),
						lineCount,
						LOC,
						eLOC
				);


			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(lineNumberReader != null)
					try {
						lineNumberReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			
		}
		
	}


	private boolean isFileForStudy(File soFile) {
		String fileName = soFile.getName();
		return fileName.endsWith("c") ||
				fileName.endsWith("cc") ||
				fileName.endsWith("h") ||
				fileName.endsWith("sh") ||
				fileName.endsWith("conf") ||
				fileName.startsWith("Makefile");
	}


	private synchronized List<RepositoryFile> getRepositoryFiles(SCMRepository repo, String hash) {
		List<RepositoryFile> files = null;
		try {
			System.err.println(hash);

			repo.getScm().checkout(hash);

			files = repo.getScm().files();
		} finally {
			repo.getScm().reset();
		}
	
		return files;
	}
	

	private boolean isProcessableHash(String hash) {
		return "2b299e61160db26b5af48a750a0de3b1f57b2df0".equals(hash) || "5848d38ac3bd9bb95a5fc31dfe8f5b2bde2792a5".equals(hash) || "65763a0dd8fcc37b47e1cbbecc5c46de0fb71a38".equals(hash);
	}


	private String readFile(File f) {
		try {
			FileInputStream input = new FileInputStream(f);
			String text = IOUtils.toString(input);
			input.close();
			return text;
		} catch (Exception e) {
			throw new RuntimeException("error reading file " + f.getAbsolutePath(), e);
		}
	}
	

	public String name() {
		return "cpp-parser";
	}

}