package br.inpe.cap.apache;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;

public class ApacheFindImportsVisitor extends ASTVisitor {
	
	private boolean foundApacheImport = false;
	
	@Override
	public boolean visit(CompilationUnit node) {
		if(node.toString().contains("org.apache")) {
			foundApacheImport = true;
//			System.out.println(node.imports());
		}
		return super.visit(node);
	}
	
	public boolean foundApacheImport() {
		return foundApacheImport;
	}
	
}