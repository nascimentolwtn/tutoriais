package br.inpe.cap.apache;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import br.com.metricminer2.domain.ChangeSet;
import br.com.metricminer2.domain.Commit;
import br.com.metricminer2.domain.Modification;
import br.com.metricminer2.listener.BreakMineRepositoryListener;
import br.com.metricminer2.listener.BreakMineVisitorListener;
import br.com.metricminer2.persistence.PersistenceMechanism;
import br.com.metricminer2.scm.CommitVisitor;
import br.com.metricminer2.scm.SCMRepository;

public class ApacheModificationsVisitor implements CommitVisitor, BreakMineVisitorListener, BreakMineRepositoryListener {

	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private boolean notifyBreakMine = false;

	private String breakMessage = "";

	private List<String> hashes;

	@Override
	public void process(SCMRepository repo, Commit commit, PersistenceMechanism writer) {
		if(hashes == null) {
			initListHashes(repo);
		}
		int currentHashPosition = hashes.indexOf(commit.getHash()) + 1;
		int hashesSize = hashes.size();
		
		float percent = 100 - ((currentHashPosition * 100) / (float) hashesSize);
		String percentMessage = repo.getLastDir()
				+ " Commits: "
				+ currentHashPosition
				+ "/"
				+ hashesSize
				+ " - "
				+ percent
				+ "%";
		
		try {
			List<Modification> modifications = commit.getModifications();
			modifications.parallelStream()
				.filter(
					(m)-> m.fileNameEndsWith(".java"))
				.forEach( 
					(m) ->	{
						String sourceCode = m.getSourceCode();
						if(sourceCode.contains("import org.apache")) {
							String apacheLib = extractApacheLib(sourceCode);
							writer.write(
									commit.getHash(),
									DATE_FORMAT.format(commit.getDate().getTime()),
									repo.getLastDir(),
									m.getFileName(),
									currentHashPosition,
									hashesSize,
									percent,
									apacheLib,
									commit.getMsg().replace("\n", "").replace(",","")
							);
							this.notifyBreakMine = true;
							this.breakMessage = "FOUND: " + percentMessage;
							freeResources(repo, hashes);
						}
					}
			);
		
		} catch (RuntimeException e) {
			this.notifyBreakMine = true;
			this.breakMessage = "ERROR in: " + percentMessage;
			throw e;
		}
		
		System.err.println(percentMessage);

	}

	private void initListHashes(SCMRepository repo) {
		List<ChangeSet> changeSets = repo.getScm().getChangeSets();
		hashes = changeSets.stream().map((cs)->cs.getId()).collect(Collectors.toList());
	}

	private void freeResources(SCMRepository repo, List<String> hashes) {
		repo.closeSCM();
		repo = null;
		hashes = null;
		System.gc();
	}

	private String extractApacheLib(String sourceCode) {
		int firstIndexOfApacheLib = sourceCode.indexOf("import org.apache");
		int indexOfSemicolon = sourceCode.indexOf(";", firstIndexOfApacheLib);
		int lengthImport = 7; // "import ".length();
		String apacheLib = sourceCode.substring(firstIndexOfApacheLib+lengthImport, indexOfSemicolon);
		return apacheLib;
	}

	@Override
	public String name() {
		return "apache-modifications";
	}

	@Override
	public boolean breakMining() {
		return notifyBreakMine;
	}

	@Override
	public String logBreakMessage() {
		return breakMessage ;
	}

}