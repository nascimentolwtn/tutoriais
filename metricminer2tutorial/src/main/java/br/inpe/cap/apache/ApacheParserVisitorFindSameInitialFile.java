package br.inpe.cap.apache;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;

import com.mystudy.JavaParserVisitor;

import br.com.metricminer2.domain.ChangeSet;
import br.com.metricminer2.domain.Commit;
import br.com.metricminer2.listener.BreakMineRepositoryListener;
import br.com.metricminer2.listener.BreakMineVisitorListener;
import br.com.metricminer2.persistence.PersistenceMechanism;
import br.com.metricminer2.scm.SCMRepository;

public class ApacheParserVisitorFindSameInitialFile extends JavaParserVisitor implements BreakMineVisitorListener, BreakMineRepositoryListener {

	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	private List<ChangeSet> changeSets;
	private int indexOf;

	private boolean notifyBreakMine = false;

	private static String fileName;
	
	@Override
	public void process(SCMRepository repo, Commit commit, PersistenceMechanism writer) {
		this.changeSets = repo.getScm().getChangeSets();
		List<ChangeSet> hashes = changeSets.stream().filter(s->s.getId().equals(commit.getHash())).collect(Collectors.toList());
		this.indexOf = changeSets.indexOf(hashes.get(0));

		super.process(repo, commit, writer);
		
		int percent = ((indexOf+1)*100)/changeSets.size();
		System.err.println(repo.getLastDir()
				+ " Commits: "
				+ (indexOf+1)
				+ "/"
				+ changeSets.size()
				+ " - "
				+ percent
				+ "%");

	}

	@Override
	protected void processFile(SCMRepository repo, Commit commit, PersistenceMechanism writer, String fileName, ByteArrayInputStream file) {
		
		try {
			String conteudo = IOUtils.toString(file);
			if(conteudo.contains("import org.apache")) {
				writer.write(
						commit.getHash(),
						DATE_FORMAT.format(commit.getDate().getTime()),
						fileName,
						(indexOf+1),
						changeSets.size()
				);
				if(ApacheParserVisitorFindSameInitialFile.fileName == null) {
					ApacheParserVisitorFindSameInitialFile.fileName = fileName;
				}
				if(fileName.equals(ApacheParserVisitorFindSameInitialFile.fileName))
					this.notifyBreakMine = true;
				file = null;
			}
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public String name() {
		return "apache-parser";
	}

	@Override
	public boolean breakMining() {
		return notifyBreakMine;
	}

	@Override
	public String logBreakMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}