package br.inpe.cap.apache;

import java.util.Arrays;
import java.util.List;

import br.com.metricminer2.MetricMiner2;
import br.com.metricminer2.RepositoryMining;
import br.com.metricminer2.Study;
import br.com.metricminer2.persistence.csv.CSVFile;
import br.com.metricminer2.scm.GitRepository;
import br.com.metricminer2.scm.commitrange.Commits;

public class ApacheProjectsStudy implements Study {

	public static void main(String[] args) {
		new MetricMiner2().start(new ApacheProjectsStudy());
		System.out.println("Finish!");
	}
	
	public void execute() {
		try {
			List<String> gitRepoUrls = Arrays.asList(
//					"https://github.com/nascimentolwtn/Neutrino"
//					,
//					"https://github.com/nascimentolwtn/aomrolemapper"
//					,
					"https://github.com/iluwatar/java-design-patterns"
//					,
//					"https://github.com/nascimentolwtn/metricminer2.git"
					);
			String rootTempGitPath = "G:\\temp\\ApacheStudy\\";
			String rootApacheStudiesPath = "G:\\HD-Games\\GitRepos\\metricminer2_studies\\";
			
//			SCMRepository[] allProjectsIn = GitRemoteRepository.allProjectsIn(gitRepoUrls, rootTempGitPath);
//			SCMRepository[] allProjectsIn = GitRemoteRepository.allProjectsIn(gitRepoUrls);
			
			new RepositoryMining()
				.in(GitRepository.allProjectsIn(rootApacheStudiesPath))
				.startingFromTheBeginning()
				.through(Commits.all())
//				.withThreads(2)
//				.process(new DevelopersVisitor(), new CSVFile(".\\study\\ApacheStudy-devs.csv"))
//				.process(new ModificationsVisitor(), new CSVFile(".\\study\\ApacheStudy-modifications.csv"))
				.process(new ApacheParserVisitor(), new CSVFile(".\\study\\apache-parser-'neo4j-apoc-procedures'.csv"))
				.mine();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
	
