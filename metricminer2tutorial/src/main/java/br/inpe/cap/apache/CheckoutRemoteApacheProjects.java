package br.inpe.cap.apache;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

import br.com.metricminer2.MetricMiner2;
import br.com.metricminer2.RepositoryMining;
import br.com.metricminer2.Study;
import br.com.metricminer2.scm.GitRemoteRepository;
import br.com.metricminer2.scm.SCMRepository;

public class CheckoutRemoteApacheProjects implements Study {

	public static void main(String[] args) {
		System.setProperty("logfilename", "apache_Checkout_All_Filtered_3500_repos01");
		new MetricMiner2().start(new CheckoutRemoteApacheProjects());
		System.out.println("Finish!");
	}

	private static File exceptionFile = new File("exceptions.log");
	
	public void execute() {
		try {
			
			Collection<String> gitRepoUrls = getRepositoryUrls();
			String rootApacheStudiesPath = "C:\\metricminer2_studies\\";
			
			gitRepoUrls.parallelStream().forEach(r -> checkoutRepository(r, rootApacheStudiesPath));
			
		} catch (Exception e) {
			throw new RuntimeException(e); 
		}
	}

	private void checkoutRepository(String gitUrl, String rootApacheStudiesPath) {
		try {
			SCMRepository singleProject = GitRemoteRepository.singleProject(gitUrl, rootApacheStudiesPath);
			new RepositoryMining()
				.in(singleProject)
				.mine();
		} catch (Exception e) {
			try {
				FileUtils.writeStringToFile(exceptionFile, e.getMessage(), true);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private Collection<String> getRepositoryUrls() throws IOException {
		return FileUtils.readLines(new File("repo_github.txt"));
	}
		
}