package br.inpe.cap.apache;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

import br.com.metricminer2.MetricMiner2;
import br.com.metricminer2.RepositoryMining;
import br.com.metricminer2.Study;
import br.com.metricminer2.persistence.csv.CSVFile;
import br.com.metricminer2.scm.GitRepository;
import br.com.metricminer2.scm.commitrange.Commits;

public class LocalApacheProjectsModificationsStudy3500 implements Study {

	private static final String APACHE_FILE_PREFIX = "apache_modifications_sumary_threadpool";
	private static final File GITHUB_DONE = new File("github_done.txt");
	private static File exceptionFile = new File("exceptions-modifications.log");

	public static void main(String[] args) {
		System.setProperty("logfilename", APACHE_FILE_PREFIX + "_run12");
		new MetricMiner2().start(new LocalApacheProjectsModificationsStudy3500());
		System.out.println("Finish!");
	}
	
	public void execute() {
		try {
			
			String rootApacheStudiesPath = "C:\\metricminer2_studies\\";
			List<String> gitRepoDirs = getRepositoryExceptDoneDirs(rootApacheStudiesPath);

			ExecutorService execRepos = Executors.newFixedThreadPool(7);
			for(String repo : gitRepoDirs) {
				execRepos.submit(() -> 
					doMining(repo));
			}
		
			execRepos.shutdown();
			execRepos.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
			
		} catch (Exception e) {
			try {
				String causeMessage = "";
				if(e.getCause() != null)
					causeMessage = e.getCause().getMessage();
				FileUtils.writeStringToFile(exceptionFile, e.getMessage() + "Cause: "
						+ causeMessage, true);
				System.gc();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void doMining(String gitUrl) {
		new RepositoryMining()
			.in(GitRepository.singleProject(gitUrl))
			.startingFromTheBeginning()
			.through(Commits.all())
//			.withThreads(3)
			.process(new ApacheModificationsVisitor(), new MultipleCSVFile(
					new CSVFile("."
						+ File.separator
						+ "study"
						+ File.separator
						+ APACHE_FILE_PREFIX
						+ ".csv")
					,
					new CSVFile("."
						+ File.separator
						+ "study"
						+ File.separator
						+ "modifications"
						+ File.separator
						+ "apache-modification-'"
						+ gitUrl.substring(gitUrl.lastIndexOf(File.separator)+1, gitUrl.length())
						+ "'.csv")))
			.mine();
		markDone(gitUrl);
		System.gc();
	}
	
	private List<String> getRepositoryExceptDoneDirs(String rootApacheStudiesPath) throws IOException {
		List<String> allDirsIn = br.com.metricminer2.util.FileUtils.getAllDirsIn(rootApacheStudiesPath);
		
		FileReader arquivo = new FileReader(GITHUB_DONE);
		BufferedReader reader = new BufferedReader(arquivo);

		String linha = reader.readLine();
		while (linha != null) {
			allDirsIn.remove(linha);
			linha = reader.readLine();
		}

		reader.close();
		arquivo.close();
		return allDirsIn;
	}
		
	private void markDone(String gitUrl) {
		try {
			FileUtils.writeStringToFile(GITHUB_DONE, gitUrl + "\n", true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
}