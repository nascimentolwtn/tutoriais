package br.inpe.cap.apache;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

import br.com.metricminer2.MetricMiner2;
import br.com.metricminer2.RepositoryMining;
import br.com.metricminer2.Study;
import br.com.metricminer2.persistence.csv.CSVFile;
import br.com.metricminer2.scm.GitRepository;
import br.com.metricminer2.scm.commitrange.Commits;

public class LocalApacheUniqueProjectModificationsStudy implements Study {

	private static final String APACHE_FILE_PREFIX = "apache_modifications_sumary_threadpool_confirm";
	private static final File GITHUB_DONE = new File("github_done.txt");
	private static File exceptionFile = new File("exceptions-modifications.log");

	public static void main(String[] args) {
		System.setProperty("logfilename", APACHE_FILE_PREFIX + "percona-xtradb-cluster_run01");
		new MetricMiner2().start(new LocalApacheUniqueProjectModificationsStudy());
		System.out.println("Finish!");
	}
	
	public void execute() {
		try {
			
			String rootApacheStudiesPath = "C:\\metricminer2_studies\\percona-xtradb-cluster";

			ExecutorService execRepos = Executors.newFixedThreadPool(8);
			execRepos.submit(() -> 
				doMining(rootApacheStudiesPath));
		
			execRepos.shutdown();
			execRepos.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
			
		} catch (Exception e) {
			try {
				String causeMessage = "";
				if(e.getCause() != null)
					causeMessage = e.getCause().getMessage();
				FileUtils.writeStringToFile(exceptionFile, e.getMessage() + "Cause: "
						+ causeMessage, true);
				System.gc();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void doMining(String gitUrl) {
		new RepositoryMining()
			.in(GitRepository.singleProject(gitUrl))
//			.startingFromTheBeginning()
			.through(Commits.all())
//			.withThreads(2)
			.process(new ApacheParserVisitor(), new MultipleCSVFile(
					new CSVFile("."
						+ File.separator
						+ "study"
						+ File.separator
						+ APACHE_FILE_PREFIX
						+ ".csv")
					,
					new CSVFile("."
						+ File.separator
						+ "study"
						+ File.separator
						+ "modifications"
						+ File.separator
						+ "apache-modification-'"
						+ gitUrl.substring(gitUrl.lastIndexOf(File.separator)+1, gitUrl.length())
						+ "'.csv")))
			.mine();
		markDone(gitUrl);
		System.gc();
	}
	
	private void markDone(String gitUrl) {
		try {
			FileUtils.writeStringToFile(GITHUB_DONE, gitUrl + "\n", true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
}