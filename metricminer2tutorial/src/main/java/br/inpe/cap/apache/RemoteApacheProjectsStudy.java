package br.inpe.cap.apache;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import br.com.metricminer2.MetricMiner2;
import br.com.metricminer2.RepositoryMining;
import br.com.metricminer2.Study;
import br.com.metricminer2.persistence.csv.CSVFile;
import br.com.metricminer2.scm.GitRemoteRepository;
import br.com.metricminer2.scm.commitrange.Commits;

public class RemoteApacheProjectsStudy implements Study {

	public static void main(String[] args) {
		System.setProperty("logfilename", "apache_top20_withBreakMine");
		new MetricMiner2().start(new RemoteApacheProjectsStudy());
		System.out.println("Finish!");
	}
	
	public void execute() {
		try {
			
			Set<String> gitRepoUrls = getRepositoryUrls();
			String rootApacheStudiesPath = "E:\\metricminer2_studies\\";
			
			gitRepoUrls.parallelStream().forEach(r -> doMining(r, rootApacheStudiesPath));
			
		} catch (Exception e) {
			throw new RuntimeException(e); 
		}
	}

	private void doMining(String gitUrl, String rootApacheStudiesPath) {
		new RepositoryMining()
			.in(GitRemoteRepository.singleProject(gitUrl, rootApacheStudiesPath))
			.startingFromTheBeginning()
			.through(Commits.all())
//			.withThreads(2)
			.process(new ApacheParserVisitor(), new CSVFile(".\\study\\apache-parser-'"
					+ gitUrl.substring(gitUrl.lastIndexOf("/")+1, gitUrl.length())
					+ "'.csv"))
			.mine();
		new RepositoryMining()
			.in(GitRemoteRepository.singleProject(gitUrl, rootApacheStudiesPath))
			.through(Commits.all())
//			.withThreads(2)
			.process(new ApacheParserVisitor(), new CSVFile(".\\study\\apache-parser-'"
					+ gitUrl.substring(gitUrl.lastIndexOf("/")+1, gitUrl.length())
					+ "'.csv"))
			.mine();
	}
	
	private Set<String> getRepositoryUrls() throws IOException {
		Set<String> listaRepositorios = new HashSet<String>();

		FileReader arquivo = new FileReader("repositories.txt");
		BufferedReader reader = new BufferedReader(arquivo);

		String linha = reader.readLine();
		while (linha != null) {
			if(!linha.startsWith("#"))
				listaRepositorios.add(linha);
			linha = reader.readLine();
		}

		arquivo.close();
		return listaRepositorios;
	}
		
}