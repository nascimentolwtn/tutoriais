package com.mystudy;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.io.IOUtils;

import br.com.metricminer2.domain.Commit;
import br.com.metricminer2.parser.jdt.JDTRunner;
import br.com.metricminer2.persistence.PersistenceMechanism;
import br.com.metricminer2.scm.CommitVisitor;
import br.com.metricminer2.scm.RepositoryFile;
import br.com.metricminer2.scm.SCMRepository;

public class JavaParserVisitor implements CommitVisitor {

	public void process(SCMRepository repo, Commit commit, PersistenceMechanism writer) {

		Map<String, ByteArrayInputStream> files = getRepositoryFiles(repo, commit.getHash());
		
		for(Map.Entry<String, ByteArrayInputStream> entry : files.entrySet()) {
			String fileName = entry.getKey();
			ByteArrayInputStream file = entry.getValue();
			processFile(repo, commit, writer, fileName, file);
		}
		
		files=null;
		System.gc();

	}

	private Map<String, ByteArrayInputStream> getRepositoryFiles(final SCMRepository repo, String hash) {
		
		ReentrantLock checkoutLock = new ReentrantLock();
		checkoutLock.lock();
		try {
		
			repo.getScm().checkout(hash);
			
			System.err.println(hash);
	
			List<RepositoryFile> repositoryFiles = repo.getScm().files();
			
			Map<String, ByteArrayInputStream> mapa = new ConcurrentHashMap<>(repositoryFiles.size());
			
			repositoryFiles.stream()
				.filter(
						(rf) -> rf.fileNameEndsWith("java"))
				.forEach(
						(frf)-> mapa.put(frf.getFullName(), new ByteArrayInputStream(readFile(frf.getFile()).getBytes())));

			return mapa;
			
		} finally {
			repo.getScm().reset();
			checkoutLock.unlock();
		}
	}
	
	protected void processFile(SCMRepository repo, Commit commit, PersistenceMechanism writer, String fileName, ByteArrayInputStream file) {
		
		NumberOfMethodsAndFieldsVisitor methodsFieldsVisitor = new NumberOfMethodsAndFieldsVisitor();
		new JDTRunner().visit(methodsFieldsVisitor, file);
		
		int methods = methodsFieldsVisitor.getQtdMethods();
		
		int fields = methodsFieldsVisitor.getQtdFields();
		
		if(methods > 0 || fields > 0)
			writer.write(
					commit.getHash(),
					fileName,
					methods,
					fields
		);
		file=null;
		
	}

	private String readFile(File f) {
		try {
			FileInputStream input = new FileInputStream(f);
			String text = IOUtils.toString(input);
			input.close();
			return text;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("error reading file " + f.getAbsolutePath(), e);
		}
	}

	public String name() {
		return "java-parser";
	}

}