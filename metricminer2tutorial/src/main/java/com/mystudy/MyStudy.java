package com.mystudy;

import br.com.metricminer2.MetricMiner2;
import br.com.metricminer2.RepositoryMining;
import br.com.metricminer2.Study;
import br.com.metricminer2.persistence.csv.CSVFile;
import br.com.metricminer2.scm.GitRepository;
import br.com.metricminer2.scm.commitrange.Commits;

public class MyStudy implements Study {

	public static void main(String[] args) {
		new MetricMiner2().start(new MyStudy());
		System.out.println("Finish!");
	}

	public void execute() {
		new RepositoryMining()
			.in(GitRepository.allProjectsIn("G:\\HD-Games\\GitRepos"))
			.through(Commits.all())
//			.withThreads(2)
			.process(new DevelopersVisitor(), new CSVFile(".\\study\\devs-onlyCommits-allLocalGitRepos.csv"))
			.process(new ModificationsVisitor(), new CSVFile(".\\study\\modifications-onlyCommits-allLocalGitRepos.csv"))
//			.process(new JavaParserVisitor(10), new CSVFile(".\\study\\java-parser-threads.csv"))
			.mine();
	}
}
	
