package com.mystudy;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;

public class NumberOfFieldsVisitor extends ASTVisitor {

	private int qty = 0;
	
	@Override
	public boolean visit(FieldDeclaration node) {
		qty++;
		return super.visit(node);
	}
	
	public int getQty() {
		return qty;
	}
}