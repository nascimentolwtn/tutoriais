package com.mystudy;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public class NumberOfMethodsAndFieldsVisitor extends ASTVisitor {

	private int qtdMethods = 0;
	
	private int qtdFields = 0;
	
	@Override
	public boolean visit(FieldDeclaration node) {
		qtdFields++;
		return super.visit(node);
	}
	
	@Override
	public boolean visit(MethodDeclaration node) {
		
		qtdMethods++;
		return super.visit(node);
	}
	
	public int getQtdMethods() {
		return qtdMethods;
	}
	
	public int getQtdFields() {
		return qtdFields;
	}
}