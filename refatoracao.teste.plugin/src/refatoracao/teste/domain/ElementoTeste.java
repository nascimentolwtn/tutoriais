package refatoracao.teste.domain;

public class ElementoTeste {
	
	private String expressao;
	private String tipo;
	
	public static final String ASSERCAO = "ASSERCAO";
	public static final String ACAO = "ACAO";

	public String getExpressao() {
		return expressao;
	}

	public String getTipo() {
		return tipo;
	}

	private void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setExpressao(String expressao) {
		this.expressao = expressao;
	}
	
	public static ElementoTeste criarElemento(String expressao){
		ElementoTeste elemento = new ElementoTeste();
		if(expressao.startsWith("assert"))
			elemento.setTipo(ASSERCAO);
		else
			elemento.setTipo(ACAO);
		elemento.setExpressao(expressao);
		return elemento;
	}

}
