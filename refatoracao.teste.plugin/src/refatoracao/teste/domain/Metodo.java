package refatoracao.teste.domain;

import java.util.ArrayList;

public class Metodo {
	
	private ArrayList<ElementoTeste> elementos = new ArrayList<ElementoTeste>();
	private String nomeMetodo; 
	private String declaracaoMetodo; 

	public ArrayList<ElementoTeste> getElementos() {
		return elementos;
	}
	
	public void addElemento(ElementoTeste elemento){
		elementos.add(elemento);
	}
	
	public void addElemento(ElementoTeste elemento, int position){
		elementos.add(position,elemento);
	}

	public String getNomeMetodo() {
		return nomeMetodo;
	}

	public void setNomeMetodo(String nomeMetodo) {
		this.nomeMetodo = nomeMetodo;
	}

	public String getDeclaracaoMetodo() {
		return declaracaoMetodo;
	}

	public void setDeclaracaoMetodo(String declaracaoMetodo) {
		this.declaracaoMetodo = declaracaoMetodo;
	}
	
	public boolean containsTeste(Metodo teste){
		int index = 0;
		for(ElementoTeste elemento : teste.getElementos()){
			if(elemento.getTipo() == ElementoTeste.ASSERCAO)
				continue;
			ElementoTeste proximaAcao = null;
			while(proximaAcao == null || !(proximaAcao.getTipo() == ElementoTeste.ACAO)){
				if(index == elementos.size())
					return false;
				proximaAcao = elementos.get(index);
				index++;
			}
			if(!proximaAcao.getExpressao().equals(elemento.getExpressao()))
				return false;
		}
		return true;
	}
	
	public void incorporarTeste(Metodo teste){
			ArrayList<ElementoTeste> novos = new ArrayList<ElementoTeste>();
			while(elementos.size() != 0){
				while(teste.getElementos().size() != 0 && !elementos.get(0).getExpressao().equals(teste.getElementos().get(0).getExpressao())){
					if(elementos.get(0).getTipo() == ElementoTeste.ASSERCAO){
						novos.add(elementos.get(0));
						elementos.remove(0);
						continue;
					}
					if(teste.getElementos().get(0).getTipo() == ElementoTeste.ASSERCAO){
						novos.add(teste.getElementos().get(0));
						teste.getElementos().remove(0);
						continue;
					}
				}
				novos.add(elementos.get(0));
				elementos.remove(0);
				if(teste.getElementos().size() > 0)
					teste.getElementos().remove(0);
			}
			elementos = novos;
		}

}
