package refatoracao.teste.domain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class SuiteTestes {
	
	private String nomeClasse;
	private String classPackage;
	private String declaracaoClasse;
	private ArrayList<String> imports = new ArrayList<String>();
	private ArrayList<String> fixtures = new ArrayList<String>();
	private ArrayList<Metodo> testes = new ArrayList<Metodo>();
	private Metodo setUp;
	private Metodo tearDown;
	private ArrayList<Metodo> metodos = new ArrayList<Metodo>();
	
	public String getDeclaracaoClasse() {
		return declaracaoClasse;
	}
	public void setDeclaracaoClasse(String declaracaoClasse) {
		this.declaracaoClasse = declaracaoClasse;
	}
	public String getNomeClasse() {
		return nomeClasse;
	}
	public void setNomeClasse(String nomeClasse) {
		this.nomeClasse = nomeClasse;
	}
	public ArrayList<Metodo> getTestes() {
		return testes;
	}
	public void addMetodo(Metodo metodo){
		if(metodo.getNomeMetodo().startsWith("test"))
			testes.add(metodo);
		else if(metodo.getNomeMetodo().equals("setUp"))
			setUp = metodo;
		else if(metodo.getNomeMetodo().equals("tearDown"))
			tearDown = metodo;
		else
			metodos.add(metodo);
	}
	public void addImport(String classImport){
		imports.add(classImport);
	}
	public void addFixture(String fixture){
		if(!fixtures.contains(fixture)){
			String[] fixtureParts = StringUtils.split(fixture); 
			fixtures.add(fixtureParts[0].trim()+" "+fixtureParts[1].trim());
		}
	}
	public String getClassPackage() {
		return classPackage;
	}
	public void setClassPackage(String classPackage) {
		this.classPackage = classPackage;
	}
	public ArrayList<String> getImports() {
		return imports;
	}
	public ArrayList<String> getFixtures() {
		return fixtures;
	}
	public List<Metodo> getTodosMetodos(){
		List<Metodo> list = new ArrayList<Metodo>();
		if(setUp != null)
			list.add(setUp);
		list.addAll(testes);
		list.addAll(metodos);
		if(tearDown != null)
			list.add(tearDown);
		return list;
	}
	public Metodo getTearDown() {
		return tearDown;
	}
	public void setTearDown(Metodo tearDown) {
		this.tearDown = tearDown;
	}
	public void setSetUp(Metodo setUp) {
		this.setUp = setUp;
	}
	public Metodo getSetUp() {
		return setUp;
	}

}
