package refatoracao.teste.parser;

import java.io.*;

import org.apache.commons.lang.StringUtils;

import refatoracao.teste.domain.*;
import refatoracao.teste.refactoracoes.*;

public class ClasseTesteParser {
	
	public SuiteTestes parse(Reader reader) throws IOException{
		SuiteTestes suite = new SuiteTestes();
		BufferedReader in = new BufferedReader(reader);
		
		String line;
		Metodo teste = null;
		
		while((line = in.readLine()) != null){
			line = StringUtils.trim(line);
			//Package
			if(line.indexOf("package") >= 0){
				suite.setClassPackage(line);
				continue;
			}
			//Imports
			if(line.indexOf("import") >= 0){
				suite.addImport(line);
				continue;
			}
			//Achando a classe
			if(line.indexOf("public class") >= 0){
				suite.setNomeClasse(getClassName(line));
				suite.setDeclaracaoClasse(line);
				continue;
			}
			//Metodo de teste
			if(line.endsWith("{")){
				teste = new Metodo();
				teste.setNomeMetodo(geMethodName(line));
				teste.setDeclaracaoMetodo(line);
				continue;
			}
			if(teste != null){
				if(line.indexOf("}") == 0){
					suite.addMetodo(teste);
					teste = null;
					continue;
				}
				if(line.indexOf("}") == line.length()-1){
					line = line.substring(0,line.length()-1);
					teste.addElemento(ElementoTeste.criarElemento(line));
					suite.addMetodo(teste);
					teste = null;
					continue;
				}
				teste.addElemento(ElementoTeste.criarElemento(line));
				continue;
			} else {
				if(line.endsWith(";"))
					suite.addFixture(line);
			}
		}
		
		return suite;
	}

	private String getClassName(String line) {
		int begin = line.indexOf("public class")+13;
		while(line.charAt(begin) == ' ')
			begin++;
		String name = line.substring(begin,line.indexOf(" ",begin));
		return name;
	}
	
	private String geMethodName(String line) {
		int begin = line.indexOf("(");
		while(line.charAt(begin) != ' ')
			begin--;
		String name = line.substring(begin+1,line.indexOf("(",begin));
		return name;
	}
	
	public void writeSuite(File file, SuiteTestes testes) throws IOException{
		PrintStream out = new PrintStream(file);
		out.println(testes.getClassPackage());
		out.println("");
		for(String imports : testes.getImports())
			out.println(imports);
		out.println("");
		out.println(testes.getDeclaracaoClasse());
		out.println("");
		for(String fixture : testes.getFixtures())
			out.println("   "+fixture);
		out.println("");
		for(Metodo metodo : testes.getTodosMetodos()){
			out.println("   "+metodo.getDeclaracaoMetodo());
			for(ElementoTeste elemento : metodo.getElementos()){
				out.println("      "+elemento.getExpressao());
			}
			out.println("   }");
			out.println("");
		}
		out.println("}");
		out.flush();
		out.close();
	}
	
	public String getCode(SuiteTestes testes) throws IOException{
		StringBuilder builder = new StringBuilder();
		builder.append(testes.getClassPackage()+"\n");
		builder.append("\n");
		for(String imports : testes.getImports())
			builder.append(imports+"\n");
		builder.append("\n");
		builder.append(testes.getDeclaracaoClasse()+"\n");
		builder.append("\n");
		for(String fixture : testes.getFixtures())
			builder.append("   "+fixture+"\n");
		builder.append("\n");
		for(Metodo metodo : testes.getTodosMetodos()){
			builder.append("   "+metodo.getDeclaracaoMetodo()+"\n");
			for(ElementoTeste elemento : metodo.getElementos()){
				builder.append("      "+elemento.getExpressao()+"\n");
			}
			builder.append("   }\n");
			builder.append("\n");
		}
		builder.append("}\n");
		return builder.toString();
	}
	
	public static void main(String[] args) throws IOException{
		ClasseTesteParser parser = new ClasseTesteParser();
		SuiteTestes testes = parser.parse(new FileReader("TestGerente.java"));
		Refatoracao ref1 = new AdicionarFixture();
		ref1.refatorar(testes);
		parser.writeSuite(new File("TestGerente2.java"), testes);
		ExtrairMetodoDeInicializacao ref2 = new ExtrairMetodoDeInicializacao();
		ref2.refatorar(testes);
		parser.writeSuite(new File("TestGerente3.java"), testes);
		ExtrairMetodoDeFinalizacao ref3 = new ExtrairMetodoDeFinalizacao();
		ref3.refatorar(testes);
		parser.writeSuite(new File("TestGerente4.java"), testes);
		UnirTestesIncrementais ref4 = new UnirTestesIncrementais();
		ref4.refatorar(testes);
		parser.writeSuite(new File("TestGerente5.java"), testes);
	}

}
