package refatoracao.teste.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import org.apache.commons.lang.StringUtils;

import refatoracao.teste.domain.ElementoTeste;
import refatoracao.teste.domain.Metodo;

public class ColocadorNumeros {

	public static void main(String[] args) throws IOException{
		BufferedReader in = new BufferedReader(new FileReader("TestGerente5.java"));
		PrintStream out = new PrintStream("Numerado.java");
		
		String line;
		int count = 1;
		while((line = in.readLine()) != null){
			if(count < 10)
				out.println("0" + count + ". "+line);
			else
				out.println(count + ". "+line);
			count++;
		}
		out.flush();
		out.close();
	}

}
