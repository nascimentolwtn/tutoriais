package refatoracao.teste.plugin.actions;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import refatoracao.teste.domain.SuiteTestes;
import refatoracao.teste.parser.ClasseTesteParser;
import refatoracao.teste.refactoracoes.Refatoracao;

public abstract class BasicRefactoringAction implements IWorkbenchWindowActionDelegate{

	private ICompilationUnit unit;

	/**
	 * The action has been activated. The argument of the
	 * method represents the 'real' action sitting
	 * in the workbench UI.
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		if(!unit.getElementName().startsWith("Test")){
			MessageDialog.openInformation(
					null,
					"Test Refatoring Plug-in",
					"The file "+unit.getElementName()+" does not contains a test class!");
		} else{
			try {
				IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(unit.getPath());
				ClasseTesteParser parser = new ClasseTesteParser();
				SuiteTestes testes = parser.parse(new InputStreamReader(file.getContents()));
				Refatoracao ref = getRefatoracao();
				ref.refatorar(testes);
				ByteArrayInputStream str = new ByteArrayInputStream(parser.getCode(testes).getBytes());
				file.setContents(str,false,false,null);
			} catch (Exception e) {
				MessageDialog.openInformation(
						null,
						"Test Refatoring Plug-in",
						"Not possible to refactor the code");
			}
		}
		
	}
	
	protected abstract Refatoracao getRefatoracao();

	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if(selection != null && selection instanceof IStructuredSelection){
			IStructuredSelection ss = (IStructuredSelection)selection;
			if(ss.getFirstElement() instanceof ICompilationUnit){
				unit = (ICompilationUnit)ss.getFirstElement();
			}
		}
		
	}

	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
	}

}