package refatoracao.teste.plugin.actions;

import java.io.*;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.internal.core.SourceType;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import refatoracao.teste.refactoracoes.AdicionarFixture;
import refatoracao.teste.refactoracoes.Refatoracao;

public class CreateFixture extends BasicRefactoringAction  {

	protected Refatoracao getRefatoracao(){
		return new AdicionarFixture();
	}
}
