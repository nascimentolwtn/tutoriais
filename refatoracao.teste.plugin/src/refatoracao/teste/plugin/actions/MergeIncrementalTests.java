package refatoracao.teste.plugin.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import refatoracao.teste.refactoracoes.*;

public class MergeIncrementalTests extends BasicRefactoringAction  {

	protected Refatoracao getRefatoracao(){
		return new UnirTestesIncrementais();
	}
}