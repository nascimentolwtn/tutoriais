package refatoracao.teste.refactoracoes;

import refatoracao.teste.domain.ElementoTeste;
import refatoracao.teste.domain.Metodo;
import refatoracao.teste.domain.SuiteTestes;

public class AdicionarFixture implements Refatoracao {

	public void refatorar(SuiteTestes suite){
		for(Metodo metodo : suite.getTestes()){
			for(ElementoTeste elemento : metodo.getElementos()){
				if(elemento.getTipo() == ElementoTeste.ACAO){
					String expr = elemento.getExpressao();
					if(expr.indexOf("=") > 0){
						String[] vars = expr.substring(0,expr.indexOf("=")).split(" ");
						if(vars.length == 2){
							suite.addFixture(vars[0]+" "+vars[1]+";");
							elemento.setExpressao(vars[1]+" ="+expr.substring(expr.indexOf("=")+1));
						}
					}
				}
			}
		}
	}
}
