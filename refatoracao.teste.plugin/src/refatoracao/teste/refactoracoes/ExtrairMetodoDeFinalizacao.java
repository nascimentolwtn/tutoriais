package refatoracao.teste.refactoracoes;

import refatoracao.teste.domain.ElementoTeste;
import refatoracao.teste.domain.Metodo;
import refatoracao.teste.domain.SuiteTestes;

public class ExtrairMetodoDeFinalizacao implements Refatoracao {

	public void refatorar(SuiteTestes suite) {
		if(suite.getTearDown() != null)
			return;
		Metodo tearDown = new Metodo();
		tearDown.setNomeMetodo("tearDown");
		tearDown.setDeclaracaoMetodo("public void tearDown(){");
		Principal: while(true){
			ElementoTeste elemento = suite.getTestes().get(0).getElementos().get(suite.getTestes().get(0).getElementos().size()-1);
			if(elemento.getTipo() == ElementoTeste.ASSERCAO)
				break;
			for(Metodo metodo : suite.getTestes()){
				if(!metodo.getElementos().get(metodo.getElementos().size()-1).getExpressao().equals(elemento.getExpressao()))
					break Principal;
			}
			for(Metodo metodo : suite.getTestes()){
				metodo.getElementos().remove(metodo.getElementos().size()-1);
			}
			tearDown.addElemento(elemento,0);
		}
		if(tearDown.getElementos().size() > 0)
			suite.setTearDown(tearDown);
	}

}
