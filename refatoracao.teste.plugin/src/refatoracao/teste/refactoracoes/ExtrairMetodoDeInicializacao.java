package refatoracao.teste.refactoracoes;

import refatoracao.teste.domain.ElementoTeste;
import refatoracao.teste.domain.Metodo;
import refatoracao.teste.domain.SuiteTestes;

public class ExtrairMetodoDeInicializacao implements Refatoracao {

	public void refatorar(SuiteTestes suite) {
		if(suite.getSetUp() != null)
			return;
		Metodo setUp = new Metodo();
		setUp.setNomeMetodo("setUp");
		setUp.setDeclaracaoMetodo("public void setUp(){");
		Principal: while(true){
			ElementoTeste elemento = suite.getTestes().get(0).getElementos().get(0);
			if(elemento.getTipo() == ElementoTeste.ASSERCAO)
				break;
			for(Metodo metodo : suite.getTestes()){
				if(!metodo.getElementos().get(0).getExpressao().equals(elemento.getExpressao()))
					break Principal;
			}
			for(Metodo metodo : suite.getTestes()){
				metodo.getElementos().remove(0);
			}
			setUp.addElemento(elemento);
		}
		if(setUp.getElementos().size() > 0)
			suite.setSetUp(setUp);
	}

}
