package refatoracao.teste.refactoracoes;

import refatoracao.teste.domain.SuiteTestes;

public interface Refatoracao {

	public abstract void refatorar(SuiteTestes suite);

}