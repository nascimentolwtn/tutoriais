package refatoracao.teste.refactoracoes;

import refatoracao.teste.domain.Metodo;
import refatoracao.teste.domain.SuiteTestes;

public class UnirTestesIncrementais implements Refatoracao {

	public void refatorar(SuiteTestes suite) {
		boolean refatorado = false;
		
		Externo: for(int i=0;i<suite.getTestes().size();i++){
			for(int j=0;j<suite.getTestes().size();j++){
				if(i != j){
					if(suite.getTestes().get(i).containsTeste(suite.getTestes().get(j))){
						Metodo testeContido = suite.getTestes().get(j);
						suite.getTestes().get(i).incorporarTeste(testeContido);
						suite.getTestes().remove(j);
						refatorado = true;
						break Externo;
					}
				}
					
			}
		}
		
		if(refatorado)
			refatorar(suite);
	}

}
